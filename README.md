# Jupyter Lab Desktop Shortcut

A `.desktop` file for Jupyter Lab so you can launch it via your DE's GUI + a GTK-based system tray to open new Jupyter Lab windows and shutdown the process.

## Install

1. Copy `jupyter-lab.desktop` to `~/.local/share/applications`
2. Copy `jupyter-lab.svg` to `~/.local/share/icons`
3. Copy `jupyter-desktop` to `~/.local/bin` (or any other folder in your `PATH`) and make sure it's executable

Thanks to [egormkn](https://github.com/egormkn) for [this Gist](https://gist.github.com/egormkn/672764e7ce3bdaf549b62a5e70eece79) that I based my `.desktop` file on.
